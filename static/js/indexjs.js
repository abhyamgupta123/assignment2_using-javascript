var detailbtnlist;

function calcTime(offset) {

    // create Date object for current location
    d = new Date();

    // convert to msec
    // add local time zone offset
    // get UTC time in msec
    utc = d.getTime() + (d.getTimezoneOffset() * 60000);

    // create new Date object for different city
    // using supplied offset
    nd = new Date(utc + (3600000 * offset));

    // return time as a string
    return nd.toLocaleString();

}

function countryinfo(countryval) {
    var id = countryval.id.split(" ");
    localStorage["cc"] = id[0];
    localStorage["imgsrc"] = id[1];
    console.log(`${localStorage["imgsrc"]} ${localStorage["cc"]}`);
    // localStorage["imgsrc"] = countryval.
}

// console.log(calcTime("+04.30"));

$("#filter").keyup(function () {

    console.log("something");
    // Retrieve the input field text and reset the count to zero
    var filter = $(this).val(),
        count = 0;

    // Loop through the comment list
    $('#countries div').each(function () {

        // console.log($(this)[0].textContent);
        //   If the list item does not contain the text phrase fade it out
        if ($(this)[0].textContent.search(new RegExp(filter, "i")) < 0) {
            $(this).hide(); // MY CHANGE

            // Show the list item if the phrase matches and increase the count by 1
        } else {
            $(this).show(); // MY CHANGE
            count++;
        }

    });

});


$.ajax({
        url: "https://restcountries.eu/rest/v2/all",
        type: 'GET',
        dataType: 'json',
    })
    .done(function (data) {
        data.forEach((item) => {
            $("#countries").append(
                `<div class="card mr-auto ml-auto" style="width: 100%; height: auto; margin-top: 1%; padding: 2%">
                    <div class="row">
                        <div class="col-lg-6 col-xm-12 col-sm-12 col-md-6">
                            <img class="card-img-top" src="${item.flag}" alt="${item.name}" style=" width: 100%; padding-left: 0%">
                        </div>
                        <div class="col-lg-6 col-xm-12 col-sm-12 align-items-centre">
                            <div class="card-body">
                                <h3 class="card-title"><strong>${item.name}</strong></h3>
                                <p class="card-text">Currency: ${item.currencies[0].name}</p>
                                <p class="card-text">Current date and time: ${calcTime(item.timezones[0].slice(3).replace(":", "."))}</p>
                                
                                <a href="#" class="mr-auto mb-auto btn btn-outline-primary" style="width: 40%">Show Map</a> 
                                <a href="detail.html" class="btn btn-outline-primary text-right float-end detailbtn" id="${item.alpha3Code} ${item.flag}" style="width: 40%">Detail</a>                           
                            </div>
                        </div>
                    </div>
                </div>`
            )
        })

        detailbtnlist = document.querySelectorAll(".btn-outline-primary");
        detailbtnlist.forEach((btn) => {
            btn.addEventListener('click', () => {
                countryinfo(btn);
                console.log(btn);
                console.log("btn clicked of country alpha id", btn.id);
            })
        });

        // console.log("detailbtn is", detailbtnlist);
        // console.log("detailbtn is", detailbtnlist[0].nextElementSibling.id);

        // console.log("data recived", data);
        // console.log("country name", data[0].alpha3Code);
        // console.log("flag", data[0].flag);
        // console.log("currencies", data[0].currencies[0].name);
        // console.log("time", data[0].timezones[0].slice(3));
        // console.log("time", calcTime(data[0].timezones[0].slice(3).replace(":", ".")));

    })
    .fail(function (e) {
        console.log("error occured while fetching data", e);
    })
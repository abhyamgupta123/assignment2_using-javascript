window.onload = function () {
    var cc = localStorage["cc"];
    var imgsrc = localStorage["imgsrc"];
    // console.log(cc, imgsrc);
    var $othercountryflags = $("#other-countries");

    $("#country-flag").attr("src", imgsrc);
    var fullurl = "https://restcountries.eu/rest/v2/alpha/" + cc;

    $.ajax({
            url: fullurl,
            type: 'GET',
            dataType: 'json',
        })
        .done(function (data) {
            // console.log(data);
            // console.log($("#country-name").text());
            var lang = "";
            borders = data["borders"];
            data["languages"].forEach((name) => {
                // console.log("name is ",this);
                if (data["languages"].length > 1) {
                    lang = lang + `${name.name}` + " and ";
                } else {
                    lang = `${name.name}`;
                }
            });
            $("#country-name").text(data["name"]);
            $("title").text(data["name"]);
            $("#country-flag").attr("alt", data["name"]);
            $("#country-info").append(
                `<p>Native Name: ${data["altSpellings"][1]}</p>
                 <p>Capital: ${data["capital"]}</p>
                 <p>Population: ${data["population"]}</p>
                 <p>Region: ${data["region"]}</p>
                 <p>Sub-region: ${data["subregion"]}</p>
                 <p>Area: ${data["area"]}</p>
                 <p>Country Code: +${data["callingCodes"][0]}</p>
                 <p>Languages: ${lang}</p>
                 <p>Currencies: ${data["currencies"][0].name}</p>
                 <p>Timezones: ${data["timezones"][0]}</p>`
            );

            var bordercodesurl = "https://restcountries.eu/rest/v2/alpha?codes=";
            data["borders"].forEach(code => {
                bordercodesurl = bordercodesurl + code + ";";
            });

            $.ajax({
                    url: bordercodesurl,
                    type: 'GET',
                    dataType: 'json',
                })
                .done(function (data) {
                    console.log(data);
                    data.forEach(element => {
                        $othercountryflags.append(`
                        <div class="col-md-4 col-lg-4 col-xs-10 col-sm-10 text-center">
                            <img class="img-responsive" src="${element.flag}" alt="${element.name}" style="width:90%; height:90%">
                        </div>
                        `);
                    })
                })
                .fail(function (e) {
                    console.log("error occured while fetching data for neighbourring country flags", e);
                });
        })
        .fail(function (e) {
            console.log("error occured while fetching data", e);
        });
}